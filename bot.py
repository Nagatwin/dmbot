import os
import discord
from discord.ext import commands
from dotenv import load_dotenv
import logging
import pickle

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

# create logger
logger = logging.getLogger("discord_bot")
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)
async def try_delete(message):
    try:
        await message.delete()
    except:
        #% No permission
        log(logger.warning, 'No permission to delete message')

async def get_dm(user):
    dm = user.dm_channel
    if dm is None:
        await user.create_dm()
        dm = user.dm_channel
    return dm

def log(logging_level, msg):
    logging_level(msg)

class React:
    def __init__(self, emoji, dm):
        self.emoji = emoji
        self.dm = dm

class ReactSet:
    def __init__(self):
        self.reacts = set()
        self.msg = 'Sample message'
    
    def add_react(self, react, dm):
        r = React(react, dm)
        self.reacts.add(r)
        return r
    
    def remove_react(self, react):
        self.reacts = set([r for r in self.reacts if r.emoji != react])
    
    def check_react(self, react):
        for r in self.reacts:
            if r.emoji == react:
                return r
        return None

rs = ReactSet()

def save_reacts():
    with open('saved_reacts.dat', 'wb') as f:
        pickle.dump(rs, f)

if os.path.exists('saved_reacts.dat'):
    with open('saved_reacts.dat', 'rb') as f:
        rs = pickle.load(f)
        if 'msg' not in rs.__dict__.keys():
            rs.msg = 'Sample message'

class MyClient(discord.Client):
    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')
            
    async def on_message(self, message):
        if message.author == self.user:
            return
        
        if 'Moderators' not in [r.name for r in message.author.roles]:
            return

        if message.content.startswith('$dmhere'):
            message_sent = await message.channel.send(rs.msg)
            for react in rs.reacts:
                await message_sent.add_reaction(react.emoji)
            await try_delete(message)

        if message.content.startswith('$set_msg'):
            msg_to_use = '\n'.join(message.content.split('\n')[1:])
            rs.msg = msg_to_use
            save_reacts()
            await message.channel.send("Message set")
            
        if message.content.startswith('$add_react'):
            reaction_to_use = message.content.split()[1]
            dm = '\n'.join(message.content.split('\n')[1:])
            react = rs.add_react(reaction_to_use, dm)
            save_reacts()
            await message.channel.send("Reaction added")
            await message.author.create_dm()
            await get_dm(user).send("Here's what the dm will look like :")
            await get_dm(user).send(react.dm)
        
        if message.content.startswith('$remove_react'):
            reaction_to_use = message.content.split()[1]
            rs.remove_react(reaction_to_use)
            save_reacts()
            await message.channel.send("Reaction removed")
            await try_delete(message)

    async def on_reaction_add(self, reaction, user):
        if reaction.message.author != self.user:
            return
        
        react = rs.check_react(reaction.emoji)
        if react is not None:
            await get_dm(user).send(react.dm)

            


client = MyClient()

client.run(TOKEN)
